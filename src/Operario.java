
public class Operario extends Empleado {

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public double getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(double auxilio) {
        this.auxilio = auxilio;
    }
    
    private String turno;
    private double auxilio;
    
    public Operario (){
        
    }
    
    public Operario (String t, double au){
        super (" Jairo "," Delgado ", 15, 850000);
        this.auxilio = au;
        this.turno = t;
    }
}
